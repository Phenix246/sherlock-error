package fr.phenix246.sherlock.error.exception;

public abstract class AbstractRuntimeException extends RuntimeException {

    private final int errorCode;

    public AbstractRuntimeException(ExceptionCode code) {
        super(code.message());
        this.errorCode = code.code();
    }

    public AbstractRuntimeException(final ExceptionCode code, final Throwable origin) {
        super(code.message(), origin);
        errorCode = code.code();
    }

    public AbstractRuntimeException(final ExceptionCode code, final Object... args) {
        super(String.format(code.message(), args));
        errorCode = code.code();
    }

    public AbstractRuntimeException(final ExceptionCode code, final Throwable origin, final Object... args) {
        super(String.format(code.message(), args), origin);
        errorCode = code.code();
    }

    public int getErrorCode() {
        return errorCode;
    }
}

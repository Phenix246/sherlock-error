package fr.phenix246.sherlock.error.validation;

import fr.phenix246.sherlock.error.exception.ExceptionCode;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Optional;

/**
 * Default implementation for {Asserter}
 * @param <EXCEPTION_TYPE> The type of exception to throw.
 */
public interface DefaultAssert<EXCEPTION_TYPE extends Throwable> extends Asserter<EXCEPTION_TYPE> {

    /**
     * Verify that the object is null
     * @param object The object to test
     * @param exceptionCode The exception to throw
     * @param args The parameters for the exception message
     * @throws EXCEPTION_TYPE if the object is not null
     */
    @Override
    default void assertNull(final Object object, final ExceptionCode exceptionCode, final Object... args) throws EXCEPTION_TYPE {
        if (object != null) {
            throw logException(exceptionCode, null, args);
        }
    }

    /**
     * Verify that the object is not null
     * @param object The object to test
     * @param exceptionCode The exception to throw
     * @param args The parameters for the exception message
     * @throws EXCEPTION_TYPE if the object is null
     */
    @Override
    default void assertNotNull(final Object object, final ExceptionCode exceptionCode, final Object... args) throws EXCEPTION_TYPE {
        if (object == null) {
            throw logException(exceptionCode, null, args);
        }
    }

    /**
     * Verify that the boolean is false
     * @param bool The boolean to test
     * @param exceptionCode The exception to throw
     * @param args The parameters for the exception message
     * @throws EXCEPTION_TYPE if the boolean is true
     */
    @Override
    default void assertFalse(final boolean bool, final ExceptionCode exceptionCode, final Object... args) throws EXCEPTION_TYPE {
        if (bool) {
            throw logException(exceptionCode, null, args);
        }
    }

    /**
     * Verify that the boolean is true
     * @param bool The boolean to test
     * @param exceptionCode The exception to throw
     * @param args The parameters for the exception message
     * @throws EXCEPTION_TYPE if the boolean is false
     */
    @Override
    default void assertTrue(final boolean bool, final ExceptionCode exceptionCode, final Object... args) throws EXCEPTION_TYPE {
        if (!bool) {
            throw logException(exceptionCode, null, args);
        }
    }

    /**
     * Verify that the two object are equals
     * @param objectA The first object
     * @param objectB The second object
     * @param exceptionCode The exception to throw
     * @param args The parameters for the exception message
     * @throws EXCEPTION_TYPE if the two objects are not equals
     */
    @Override
    default void assertEquals(final Object objectA, final Object objectB, final ExceptionCode exceptionCode, final Object... args) throws EXCEPTION_TYPE {
        if (!((objectA == null && objectB == null) || (objectA != null && objectA.equals(objectB)))) {
            throw logException(exceptionCode, null, args);
        }
    }

    /**
     * Verify that the two value are equals
     * @param valueA The first value
     * @param valueB The second value
     * @param exceptionCode The exception to throw
     * @param args The parameters for the exception message
     * @throws EXCEPTION_TYPE if the two values are not equals
     */
    @Override
    default void assertEquals(final boolean valueA, final boolean valueB, final ExceptionCode exceptionCode, final Object... args) throws EXCEPTION_TYPE {
        if (valueA != valueB) {
            throw logException(exceptionCode, null, args);
        }
    }

    /**
     * Verify that the two value are equals
     * @param valueA The first value
     * @param valueB The second value
     * @param exceptionCode The exception to throw
     * @param args The parameters for the exception message
     * @throws EXCEPTION_TYPE if the two values are not equals
     */
    @Override
    default void assertEquals(final byte valueA, final byte valueB, final ExceptionCode exceptionCode, final Object... args) throws EXCEPTION_TYPE {
        if (valueA != valueB) {
            throw logException(exceptionCode, null, args);
        }
    }

    /**
     * Verify that the two value are equals
     * @param valueA The first value
     * @param valueB The second value
     * @param exceptionCode The exception to throw
     * @param args The parameters for the exception message
     * @throws EXCEPTION_TYPE if the two values are not equals
     */
    @Override
    default void assertEquals(final short valueA, final short valueB, final ExceptionCode exceptionCode, final Object... args) throws EXCEPTION_TYPE {
        if (valueA != valueB) {
            throw logException(exceptionCode, null, args);
        }
    }

    /**
     * Verify that the two value are equals
     * @param valueA The first value
     * @param valueB The second value
     * @param exceptionCode The exception to throw
     * @param args The parameters for the exception message
     * @throws EXCEPTION_TYPE if the two values are not equals
     */
    @Override
    default void assertEquals(final int valueA, final int valueB, final ExceptionCode exceptionCode, final Object... args) throws EXCEPTION_TYPE {
        if (valueA != valueB) {
            throw logException(exceptionCode, null, args);
        }
    }

    /**
     * Verify that the two value are equals
     * @param valueA The first value
     * @param valueB The second value
     * @param exceptionCode The exception to throw
     * @param args The parameters for the exception message
     * @throws EXCEPTION_TYPE if the two values are not equals
     */
    @Override
    default void assertEquals(final long valueA, final long valueB, final ExceptionCode exceptionCode, final Object... args) throws EXCEPTION_TYPE {
        if (valueA != valueB) {
            throw logException(exceptionCode, null, args);
        }
    }

    /**
     * Verify that the two value are equals
     * @param valueA The first value
     * @param valueB The second value
     * @param exceptionCode The exception to throw
     * @param args The parameters for the exception message
     * @throws EXCEPTION_TYPE if the two values are not equals
     */
    @Override
    default void assertEquals(final float valueA, final float valueB, final ExceptionCode exceptionCode, final Object... args) throws EXCEPTION_TYPE {
        if (valueA != valueB) {
            throw logException(exceptionCode, null, args);
        }
    }

    /**
     * Verify that the two value are equals
     * @param valueA The first value
     * @param valueB The second value
     * @param epsilon The epsilon value
     * @param exceptionCode The exception to throw
     * @param args The parameters for the exception message
     * @throws EXCEPTION_TYPE if the two values are not equals
     */
    @Override
    default void assertEquals(final float valueA, final float valueB, final float epsilon, final ExceptionCode exceptionCode, final Object... args) throws EXCEPTION_TYPE {
        if (valueA - valueB >= epsilon && valueB - valueA >= epsilon) {
            throw logException(exceptionCode, null, args);
        }
    }

    /**
     * Verify that the two value are equals
     * @param valueA The first value
     * @param valueB The second value
     * @param exceptionCode The exception to throw
     * @param args The parameters for the exception message
     * @throws EXCEPTION_TYPE if the two values are not equals
     */
    @Override
    default void assertEquals(final double valueA, final double valueB, final ExceptionCode exceptionCode, final Object... args) throws EXCEPTION_TYPE {
        if (valueA != valueB) {
            throw logException(exceptionCode, null, args);
        }
    }

    /**
     * Verify that the two value are equals
     * @param valueA The first value
     * @param valueB The second value
     * @param epsilon The epsilon value
     * @param exceptionCode The exception to throw
     * @param args The parameters for the exception message
     * @throws EXCEPTION_TYPE if the two values are not equals
     */
    @Override
    default void assertEquals(final double valueA, final double valueB, final double epsilon, final ExceptionCode exceptionCode, final Object... args) throws EXCEPTION_TYPE {
        if (valueA - valueB >= epsilon && valueB - valueA >= epsilon) {
            throw logException(exceptionCode, null, args);
        }
    }

    /**
     * Verify that the two value are equals
     * @param valueA The first value
     * @param valueB The second value
     * @param exceptionCode The exception to throw
     * @param args The parameters for the exception message
     * @throws EXCEPTION_TYPE if the two values are not equals
     */
    @Override
    default void assertEquals(final BigInteger valueA, final BigInteger valueB, final ExceptionCode exceptionCode, final Object... args) throws EXCEPTION_TYPE {
        if (!((valueA == null && valueB == null) || (valueA != null && valueA.equals(valueB)))) {
            throw logException(exceptionCode, null, args);
        }
    }

    /**
     * Verify that the two value are equals
     * @param valueA The first value
     * @param valueB The second value
     * @param exceptionCode The exception to throw
     * @param args The parameters for the exception message
     * @throws EXCEPTION_TYPE if the two values are not equals
     */
    @Override
    default void assertEquals(final BigDecimal valueA, final BigDecimal valueB, final ExceptionCode exceptionCode, final Object... args) throws EXCEPTION_TYPE {
        if (!((valueA == null && valueB == null) || (valueA != null && valueA.equals(valueB)))) {
            throw logException(exceptionCode, null, args);
        }
    }

    /**
     * Verify that the two object are not equals
     * @param objectA The first object
     * @param objectB The second object
     * @param exceptionCode The exception to throw
     * @param args The parameters for the exception message
     * @throws EXCEPTION_TYPE if the two objects are equals
     */
    @Override
    default void assertNotEquals(final Object objectA, final Object objectB, final ExceptionCode exceptionCode, final Object... args) throws EXCEPTION_TYPE {
        if ((objectA == null && objectB == null) || (objectA != null && objectA.equals(objectB))) {
            throw logException(exceptionCode, null, args);
        }
    }

    /**
     * Verify that the two value are not equals
     * @param valueA The first value
     * @param valueB The second value
     * @param exceptionCode The exception to throw
     * @param args The parameters for the exception message
     * @throws EXCEPTION_TYPE if the two values are not equals
     */
    @Override
    default void assertNotEquals(final boolean valueA, final boolean valueB, final ExceptionCode exceptionCode, final Object... args) throws EXCEPTION_TYPE {
        if (valueA == valueB) {
            throw logException(exceptionCode, null, args);
        }
    }

    /**
     * Verify that the two value are not equals
     * @param valueA The first value
     * @param valueB The second value
     * @param exceptionCode The exception to throw
     * @param args The parameters for the exception message
     * @throws EXCEPTION_TYPE if the two values are not equals
     */
    @Override
    default void assertNotEquals(final byte valueA, final byte valueB, final ExceptionCode exceptionCode, final Object... args) throws EXCEPTION_TYPE {
        if (valueA == valueB) {
            throw logException(exceptionCode, null, args);
        }
    }

    /**
     * Verify that the two value are not equals
     * @param valueA The first value
     * @param valueB The second value
     * @param exceptionCode The exception to throw
     * @param args The parameters for the exception message
     * @throws EXCEPTION_TYPE if the two values are not equals
     */
    @Override
    default void assertNotEquals(final short valueA, final short valueB, final ExceptionCode exceptionCode, final Object... args) throws EXCEPTION_TYPE {
        if (valueA == valueB) {
            throw logException(exceptionCode, null, args);
        }
    }

    /**
     * Verify that the two value are not equals
     * @param valueA The first value
     * @param valueB The second value
     * @param exceptionCode The exception to throw
     * @param args The parameters for the exception message
     * @throws EXCEPTION_TYPE if the two values are not equals
     */
    @Override
    default void assertNotEquals(final int valueA, final int valueB, final ExceptionCode exceptionCode, final Object... args) throws EXCEPTION_TYPE {
        if (valueA == valueB) {
            throw logException(exceptionCode, null, args);
        }
    }

    /**
     * Verify that the two value are not equals
     * @param valueA The first value
     * @param valueB The second value
     * @param exceptionCode The exception to throw
     * @param args The parameters for the exception message
     * @throws EXCEPTION_TYPE if the two values are not equals
     */
    @Override
    default void assertNotEquals(final long valueA, final long valueB, final ExceptionCode exceptionCode, final Object... args) throws EXCEPTION_TYPE {
        if (valueA == valueB) {
            throw logException(exceptionCode, null, args);
        }
    }

    /**
     * Verify that the two value are not equals
     * @param valueA The first value
     * @param valueB The second value
     * @param exceptionCode The exception to throw
     * @param args The parameters for the exception message
     * @throws EXCEPTION_TYPE if the two values are not equals
     */
    @Override
    default void assertNotEquals(final float valueA, final float valueB, final ExceptionCode exceptionCode, final Object... args) throws EXCEPTION_TYPE {
        if (valueA == valueB) {
            throw logException(exceptionCode, null, args);
        }
    }

    /**
     * Verify that the two value are not equals
     * @param valueA The first value
     * @param valueB The second value
     * @param epsilon The epsilon value
     * @param exceptionCode The exception to throw
     * @param args The parameters for the exception message
     * @throws EXCEPTION_TYPE if the two values are not equals
     */
    @Override
    default void assertNotEquals(final float valueA, final float valueB, final float epsilon,  final ExceptionCode exceptionCode, final Object... args) throws EXCEPTION_TYPE {
        if (valueA - valueB <= epsilon && valueB - valueA <= epsilon) {
            throw logException(exceptionCode, null, args);
        }
    }

    /**
     * Verify that the two value are not equals
     * @param valueA The first value
     * @param valueB The second value
     * @param exceptionCode The exception to throw
     * @param args The parameters for the exception message
     * @throws EXCEPTION_TYPE if the two values are not equals
     */
    @Override
    default void assertNotEquals(final double valueA, final double valueB, final ExceptionCode exceptionCode, final Object... args) throws EXCEPTION_TYPE {
        if (valueA == valueB) {
            throw logException(exceptionCode, null, args);
        }
    }

    /**
     * Verify that the two value are not equals
     * @param valueA The first value
     * @param valueB The second value
     * @param epsilon The epsilon value
     * @param exceptionCode The exception to throw
     * @param args The parameters for the exception message
     * @throws EXCEPTION_TYPE if the two values are not equals
     */
    @Override
    default void assertNotEquals(final double valueA, final double valueB, final double epsilon, final ExceptionCode exceptionCode, final Object... args) throws EXCEPTION_TYPE {
        if (valueA - valueB <= epsilon && valueB - valueA <= epsilon) {
            throw logException(exceptionCode, null, args);
        }
    }

    /**
     * Verify that the two value are not equals
     * @param valueA The first value
     * @param valueB The second value
     * @param exceptionCode The exception to throw
     * @param args The parameters for the exception message
     * @throws EXCEPTION_TYPE if the two values are not equals
     */
    default void assertNotEquals(final BigInteger valueA, final BigInteger valueB, final ExceptionCode exceptionCode, final Object... args) throws EXCEPTION_TYPE {
        if ((valueA == null && valueB == null) || (valueA != null && valueA.equals(valueB))) {
            throw logException(exceptionCode, null, args);
        }
    }

    /**
     * Verify that the two value are not equals
     * @param valueA The first value
     * @param valueB The second value
     * @param exceptionCode The exception to throw
     * @param args The parameters for the exception message
     * @throws EXCEPTION_TYPE if the two values are not equals
     */
    @Override
    default void assertNotEquals(final BigDecimal valueA, final BigDecimal valueB, final ExceptionCode exceptionCode, final Object... args) throws EXCEPTION_TYPE {
        if ((valueA == null && valueB == null) || (valueA != null && valueA.equals(valueB))) {
            throw logException(exceptionCode, null, args);
        }
    }

    /**
     * Verify that the object is an instance of the class provided
     * @param object The object to test
     * @param clazz The class the object must be an instanceof
     * @param exceptionCode The exception to throw
     * @param args The parameters for the exception message
     * @throws EXCEPTION_TYPE if the object is not an instance of the class
     */
    @Override
    default void assertInstanceOf(final Object object, Class<?> clazz, final ExceptionCode exceptionCode, final Object... args) throws EXCEPTION_TYPE {
        if (clazz == null || !clazz.isInstance(object)) {
            throw logException(exceptionCode, null, args);
        }
    }

    /**
     * Verify that the object is not an instance of the class provided
     * @param object The object to test
     * @param clazz The class the object must not be an instanceof
     * @param exceptionCode The exception to throw
     * @param args The parameters for the exception message
     * @throws EXCEPTION_TYPE if the object is an instance of the class
     */
    @Override
    default void assertNotInstanceOf(final Object object, Class<?> clazz, final ExceptionCode exceptionCode, final Object... args) throws EXCEPTION_TYPE {
        if (clazz == null || clazz.isInstance(object)) {
            throw logException(exceptionCode, null, args);
        }
    }

    /**
     * Verify that the collection is empty
     * @param collection The collection to test
     * @param exceptionCode The exception to throw
     * @param args The parameters for the exception message
     * @param <ELEMENT_TYPE> The type of element in the collection
     * @throws EXCEPTION_TYPE if the collection is not empty
     */
    @Override
    default <ELEMENT_TYPE> void asserIsEmpty(final Collection<ELEMENT_TYPE> collection, final ExceptionCode exceptionCode, final Object... args) throws EXCEPTION_TYPE {
        if(collection == null || !collection.isEmpty()) {
            throw logException(exceptionCode, null, args);
        }
    }

    /**
     * Verify that the collection is not empty
     * @param collection The collection to test
     * @param exceptionCode The exception to throw
     * @param args The parameters for the exception message
     * @param <ELEMENT_TYPE> The type of element in the collection
     * @throws EXCEPTION_TYPE if the collection is empty
     */
    @Override
    default <ELEMENT_TYPE> void asserNotIsEmpty(final Collection<ELEMENT_TYPE> collection, final ExceptionCode exceptionCode, final Object... args) throws EXCEPTION_TYPE {
        if(collection != null && collection.isEmpty()) {
            throw logException(exceptionCode, null, args);
        }
    }

    /**
     * Verify that the optional is not empty
     * @param optional The optional to test
     * @param exceptionCode The exception to throw
     * @param args The parameters for the exception message
     * @param <ELEMENT_TYPE> The type of element of the optional
     * @throws EXCEPTION_TYPE if the optional is empty
     */
    @Override
    default <ELEMENT_TYPE> ELEMENT_TYPE assertIsPresent(final Optional<ELEMENT_TYPE> optional, final ExceptionCode exceptionCode, final Object... args) throws EXCEPTION_TYPE {
        if(optional == null || optional.isEmpty()) {
            throw logException(exceptionCode, null, args);
        } else {
            return optional.get();
        }
    }

    /**
     * Verify that the optional is empty
     * @param optional The optional to test
     * @param exceptionCode The exception to throw
     * @param args The parameters for the exception message
     * @param <ELEMENT_TYPE> The type of element of the optional
     * @throws EXCEPTION_TYPE if the optional is not empty
     */
    @Override
    default <ELEMENT_TYPE> void assertNotIsPresent(final Optional<ELEMENT_TYPE> optional, final ExceptionCode exceptionCode, final Object... args) throws EXCEPTION_TYPE {
        if(optional != null && optional.isPresent()) {
            throw logException(exceptionCode, null, args);
        }
    }

    /**
     * Verify that the value is greater or equals to the min value
     * @param value The value to test
     * @param min The minimum value
     * @param exceptionCode The exception to throw
     * @param args The parameters for the exception message
     * @throws EXCEPTION_TYPE if the value is lower than the min value
     */
    @Override
    default void assertMin(final byte value, final byte min, final ExceptionCode exceptionCode, final Object... args) throws EXCEPTION_TYPE {
        if(value < min) {
            throw logException(exceptionCode, null, args);
        }
    }

    /**
     * Verify that the value is greater or equals to the min value
     * @param value The value to test
     * @param min The minimum value
     * @param exceptionCode The exception to throw
     * @param args The parameters for the exception message
     * @throws EXCEPTION_TYPE if the value is lower than the min value
     */
    @Override
    default void assertMin(final short value, final short min, final ExceptionCode exceptionCode, final Object... args) throws EXCEPTION_TYPE {
        if(value < min) {
            throw logException(exceptionCode, null, args);
        }
    }

    /**
     * Verify that the value is greater or equals to the min value
     * @param value The value to test
     * @param min The minimum value
     * @param exceptionCode The exception to throw
     * @param args The parameters for the exception message
     * @throws EXCEPTION_TYPE if the value is lower than the min value
     */
    @Override
    default void assertMin(final int value, final int min, final ExceptionCode exceptionCode, final Object... args) throws EXCEPTION_TYPE {
        if(value < min) {
            throw logException(exceptionCode, null, args);
        }
    }

    /**
     * Verify that the value is greater or equals to the min value
     * @param value The value to test
     * @param min The minimum value
     * @param exceptionCode The exception to throw
     * @param args The parameters for the exception message
     * @throws EXCEPTION_TYPE if the value is lower than the min value
     */
    @Override
    default void assertMin(final long value, final long min, final ExceptionCode exceptionCode, final Object... args) throws EXCEPTION_TYPE {
        if(value < min) {
            throw logException(exceptionCode, null, args);
        }
    }

    /**
     * Verify that the value is greater or equals to the min value
     * @param value The value to test
     * @param min The minimum value
     * @param exceptionCode The exception to throw
     * @param args The parameters for the exception message
     * @throws EXCEPTION_TYPE if the value is lower than the min value
     */
    @Override
    default void assertMin(final float value, final float min, final ExceptionCode exceptionCode, final Object... args) throws EXCEPTION_TYPE {
        if(value < min) {
            throw logException(exceptionCode, null, args);
        }
    }

    /**
     * Verify that the value is greater or equals to the min value
     * @param value The value to test
     * @param min The minimum value
     * @param exceptionCode The exception to throw
     * @param args The parameters for the exception message
     * @throws EXCEPTION_TYPE if the value is lower than the min value
     */
    @Override
    default void assertMin(final double value, final double min, final ExceptionCode exceptionCode, final Object... args) throws EXCEPTION_TYPE {
        if(value < min) {
            throw logException(exceptionCode, null, args);
        }
    }

    /**
     * Verify that the value is lower or equals to the max value
     * @param value The value to test
     * @param max The maximum value
     * @param exceptionCode The exception to throw
     * @param args The parameters for the exception message
     * @throws EXCEPTION_TYPE if the value is greater than the max value
     */
    @Override
    default void assertMax(final byte value, final byte max, final ExceptionCode exceptionCode, final Object... args) throws EXCEPTION_TYPE {
        if(value > max) {
            throw logException(exceptionCode, null, args);
        }
    }

    /**
     * Verify that the value is lower or equals to the max value
     * @param value The value to test
     * @param max The maximum value
     * @param exceptionCode The exception to throw
     * @param args The parameters for the exception message
     * @throws EXCEPTION_TYPE if the value is greater than the max value
     */
    @Override
    default void assertMax(final short value, final byte max, final ExceptionCode exceptionCode, final Object... args) throws EXCEPTION_TYPE {
        if(value > max) {
            throw logException(exceptionCode, null, args);
        }
    }

    /**
     * Verify that the value is lower or equals to the max value
     * @param value The value to test
     * @param max The maximum value
     * @param exceptionCode The exception to throw
     * @param args The parameters for the exception message
     * @throws EXCEPTION_TYPE if the value is greater than the max value
     */
    @Override
    default void assertMax(final int value, final byte max, final ExceptionCode exceptionCode, final Object... args) throws EXCEPTION_TYPE {
        if(value > max) {
            throw logException(exceptionCode, null, args);
        }
    }

    /**
     * Verify that the value is lower or equals to the max value
     * @param value The value to test
     * @param max The maximum value
     * @param exceptionCode The exception to throw
     * @param args The parameters for the exception message
     * @throws EXCEPTION_TYPE if the value is greater than the max value
     */
    @Override
    default void assertMax(final long value, final byte max, final ExceptionCode exceptionCode, final Object... args) throws EXCEPTION_TYPE {
        if(value > max) {
            throw logException(exceptionCode, null, args);
        }
    }

    /**
     * Verify that the value is lower or equals to the max value
     * @param value The value to test
     * @param max The maximum value
     * @param exceptionCode The exception to throw
     * @param args The parameters for the exception message
     * @throws EXCEPTION_TYPE if the value is greater than the max value
     */
    @Override
    default void assertMax(final float value, final byte max, final ExceptionCode exceptionCode, final Object... args) throws EXCEPTION_TYPE {
        if(value > max) {
            throw logException(exceptionCode, null, args);
        }
    }

    /**
     * Verify that the value is lower or equals to the max value
     * @param value The value to test
     * @param max The maximum value
     * @param exceptionCode The exception to throw
     * @param args The parameters for the exception message
     * @throws EXCEPTION_TYPE if the value is greater than the max value
     */
    @Override
    default void assertMax(final double value, final byte max, final ExceptionCode exceptionCode, final Object... args) throws EXCEPTION_TYPE {
        if(value > max) {
            throw logException(exceptionCode, null, args);
        }
    }

    /**
     * Verify that the value is between the min and the max value inclusive
     * @param value The value to test
     * @param min The minimum value
     * @param max The maximum value
     * @param exceptionCode The exception to throw
     * @param args The parameters for the exception message
     * @throws EXCEPTION_TYPE if the value is lower than the min value or greater than the max value
     */
    @Override
    default void assertBetweenInclusive(final byte value, final byte min, final byte max, final ExceptionCode exceptionCode, final Object... args) throws EXCEPTION_TYPE {
        if(value < min || value > max) {
            throw logException(exceptionCode, null, args);
        }
    }

    /**
     * Verify that the value is between the min and the max value inclusive
     * @param value The value to test
     * @param min The minimum value
     * @param max The maximum value
     * @param exceptionCode The exception to throw
     * @param args The parameters for the exception message
     * @throws EXCEPTION_TYPE if the value is lower than the min value or greater than the max value
     */
    @Override
    default void assertBetweenInclusive(final short value, final short min, final short max, final ExceptionCode exceptionCode, final Object... args) throws EXCEPTION_TYPE {
        if(value < min || value > max) {
            throw logException(exceptionCode, null, args);
        }
    }

    /**
     * Verify that the value is between the min and the max value inclusive
     * @param value The value to test
     * @param min The minimum value
     * @param max The maximum value
     * @param exceptionCode The exception to throw
     * @param args The parameters for the exception message
     * @throws EXCEPTION_TYPE if the value is lower than the min value or greater than the max value
     */
    @Override
    default void assertBetweenInclusive(final int value, final int min, final int max, final ExceptionCode exceptionCode, final Object... args) throws EXCEPTION_TYPE {
        if(value < min || value > max) {
            throw logException(exceptionCode, null, args);
        }
    }

    /**
     * Verify that the value is between the min and the max value inclusive
     * @param value The value to test
     * @param min The minimum value
     * @param max The maximum value
     * @param exceptionCode The exception to throw
     * @param args The parameters for the exception message
     * @throws EXCEPTION_TYPE if the value is lower than the min value or greater than the max value
     */
    @Override
    default void assertBetweenInclusive(final long value, final long min, final long max, final ExceptionCode exceptionCode, final Object... args) throws EXCEPTION_TYPE {
        if(value < min || value > max) {
            throw logException(exceptionCode, null, args);
        }
    }

    /**
     * Verify that the value is between the min and the max value inclusive
     * @param value The value to test
     * @param min The minimum value
     * @param max The maximum value
     * @param exceptionCode The exception to throw
     * @param args The parameters for the exception message
     * @throws EXCEPTION_TYPE if the value is lower than the min value or greater than the max value
     */
    @Override
    default void assertBetweenInclusive(final float value, final float min, final float max, final ExceptionCode exceptionCode, final Object... args) throws EXCEPTION_TYPE {
        if(value < min || value > max) {
            throw logException(exceptionCode, null, args);
        }
    }

    /**
     * Verify that the value is between the min and the max value inclusive
     * @param value The value to test
     * @param min The minimum value
     * @param max The maximum value
     * @param exceptionCode The exception to throw
     * @param args The parameters for the exception message
     * @throws EXCEPTION_TYPE if the value is lower than the min value or greater than the max value
     */
    @Override
    default void assertBetweenInclusive(final double value, final double min, final double max, final ExceptionCode exceptionCode, final Object... args) throws EXCEPTION_TYPE {
        if(value < min || value > max) {
            throw logException(exceptionCode, null, args);
        }
    }

    /**
     * Verify that the value is between the min and the max value exclusive
     * @param value The value to test
     * @param min The minimum value
     * @param max The maximum value
     * @param exceptionCode The exception to throw
     * @param args The parameters for the exception message
     * @throws EXCEPTION_TYPE if the value is lower than the min value or greater than the max value inclusive
     */
    @Override
    default void assertBetweenExclusive(final byte value, final byte min, final byte max, final ExceptionCode exceptionCode, final Object... args) throws EXCEPTION_TYPE {
        if(value <= min || value >= max) {
            throw logException(exceptionCode, null, args);
        }
    }

    /**
     * Verify that the value is between the min and the max value exclusive
     * @param value The value to test
     * @param min The minimum value
     * @param max The maximum value
     * @param exceptionCode The exception to throw
     * @param args The parameters for the exception message
     * @throws EXCEPTION_TYPE if the value is lower than the min value or greater than the max value inclusive
     */
    @Override
    default void assertBetweenExclusive(final short value, final short min, final short max, final ExceptionCode exceptionCode, final Object... args) throws EXCEPTION_TYPE {
        if(value <= min || value >= max) {
            throw logException(exceptionCode, null, args);
        }
    }

    /**
     * Verify that the value is between the min and the max value exclusive
     * @param value The value to test
     * @param min The minimum value
     * @param max The maximum value
     * @param exceptionCode The exception to throw
     * @param args The parameters for the exception message
     * @throws EXCEPTION_TYPE if the value is lower than the min value or greater than the max value inclusive
     */
    @Override
    default void assertBetweenExclusive(final int value, final int min, final int max, final ExceptionCode exceptionCode, final Object... args) throws EXCEPTION_TYPE {
        if(value <= min || value >= max) {
            throw logException(exceptionCode, null, args);
        }
    }

    /**
     * Verify that the value is between the min and the max value exclusive
     * @param value The value to test
     * @param min The minimum value
     * @param max The maximum value
     * @param exceptionCode The exception to throw
     * @param args The parameters for the exception message
     * @throws EXCEPTION_TYPE if the value is lower than the min value or greater than the max value inclusive
     */
    @Override
    default void assertBetweenExclusive(final long value, final long min, final long max, final ExceptionCode exceptionCode, final Object... args) throws EXCEPTION_TYPE {
        if(value <= min || value >= max) {
            throw logException(exceptionCode, null, args);
        }
    }

    /**
     * Verify that the value is between the min and the max value exclusive
     * @param value The value to test
     * @param min The minimum value
     * @param max The maximum value
     * @param exceptionCode The exception to throw
     * @param args The parameters for the exception message
     * @throws EXCEPTION_TYPE if the value is lower than the min value or greater than the max value inclusive
     */
    @Override
    default void assertBetweenExclusive(final float value, final float min, final float max, final ExceptionCode exceptionCode, final Object... args) throws EXCEPTION_TYPE {
        if(value <= min || value >= max) {
            throw logException(exceptionCode, null, args);
        }
    }

    /**
     * Verify that the value is between the min and the max value exclusive
     * @param value The value to test
     * @param min The minimum value
     * @param max The maximum value
     * @param exceptionCode The exception to throw
     * @param args The parameters for the exception message
     * @throws EXCEPTION_TYPE if the value is lower than the min value or greater than the max value inclusive
     */
    @Override
    default void assertBetweenExclusive(final double value, final double min, final double max, final ExceptionCode exceptionCode, final Object... args) throws EXCEPTION_TYPE {
        if(value <= min || value >= max) {
            throw logException(exceptionCode, null, args);
        }
    }

    /**
     * Verify that the string is blank
     * @param value The string to test
     * @param exceptionCode The exception to throw
     * @param args The parameters for the exception message
     * @throws EXCEPTION_TYPE if the string is not blank
     */
    @Override
    default void assertBlank(final String value, final ExceptionCode exceptionCode, final Object... args) throws EXCEPTION_TYPE {
        if(value == null || value.isBlank()) {
            throw logException(exceptionCode, null, args);
        }
    }

    /**
     * Verify that the string is not blank
     * @param value The string to test
     * @param exceptionCode The exception to throw
     * @param args The parameters for the exception message
     * @throws EXCEPTION_TYPE if the string is blank
     */
    @Override
    default void assertNotBlank(final String value, final ExceptionCode exceptionCode, final Object... args) throws EXCEPTION_TYPE {
        if(value != null && !value.isBlank()) {
            throw logException(exceptionCode, null, args);
        }
    }

    /**
     * Verify that the string has a minimum length
     * @param value The string to test
     * @param minLength The minimum length for the string
     * @param exceptionCode The exception to throw
     * @param args The parameters for the exception message
     * @throws EXCEPTION_TYPE if the string is too short
     */
    @Override
    default void assertMinLength(final String value, final int minLength, final ExceptionCode exceptionCode, final Object... args) throws EXCEPTION_TYPE {
        if(value == null || value.length() < minLength) {
            throw logException(exceptionCode, null, args);
        }
    }

    /**
     * Verify that the string don't exceed a maximum length
     * @param value The string to test
     * @param maxLength The maximum length for the string
     * @param exceptionCode The exception to throw
     * @param args The parameters for the exception message
     * @throws EXCEPTION_TYPE if the string is too long
     */
    @Override
    default void assertMaxLength(final String value, final int maxLength, final ExceptionCode exceptionCode, final Object... args) throws EXCEPTION_TYPE {
        if(value == null || value.length() > maxLength) {
            throw logException(exceptionCode, null, args);
        }
    }

    /**
     * Log and throw the exception
     * @param exceptionCode The exception to throw
     * @param source The source exception
     * @param args The parameters for the exception message
     * @return EXCEPTION_TYPE
     */
    default EXCEPTION_TYPE logException(final ExceptionCode exceptionCode, final Throwable source, final Object... args) {
        log(exceptionCode, source, args);
        return exception(exceptionCode, source, args);
    }

    /**
     * Log the exception
     * @param exceptionCode The exception to throw
     * @param source The source exception
     * @param args The parameters for the exception message
     */
    default void log(final ExceptionCode exceptionCode, final Throwable source, final Object... args) {
        // do nothing up to be implemented by final user
    }

    /**
     * Create the exception object
     * @param exceptionCode The exception to throw
     * @param source The source exception
     * @param args The parameters for the exception message
     * @return The created exception object
     */
    EXCEPTION_TYPE exception(final ExceptionCode exceptionCode, final Throwable source, final Object... args);
}

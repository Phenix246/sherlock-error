package fr.phenix246.sherlock.error.wrap;

import fr.phenix246.sherlock.error.exception.ExceptionCode;

import java.util.function.Supplier;

/**
 * Exception wrapper. Wrap any exception with the provided one
 * @param <EXCEPTION_TYPE> The exception wrapping any other exception.
 */
public interface DefaultExceptionWrapper<EXCEPTION_TYPE extends Throwable> extends ExceptionWrapper<EXCEPTION_TYPE> {

    /**
     * Call the function and wrap any exception with the provided one
     * @param action The function to execute
     * @param exceptionCode The exception to throw
     * @param args The parameters for the exception message
     * @return The result of the function
     * @param <T> The result type
     * @throws EXCEPTION_TYPE if another exception is thrown
     */
    default <T> T call(Supplier<T> action, final ExceptionCode exceptionCode, final Object... args) throws EXCEPTION_TYPE {
        try {
            return action.get();
        } catch(Exception e) {
            throw exception(exceptionCode, e, args);
        }
    }

    /**
     * Call the function and wrap any exception with the provided one
     * @param action The function to execute
     * @param exceptionCode The exception to throw
     * @param args The parameters for the exception message
     * @throws EXCEPTION_TYPE if another exception is thrown
     */
    default void call(Runnable action, final ExceptionCode exceptionCode, final Object... args) throws EXCEPTION_TYPE {
        try {
            action.run();
        } catch(Exception e) {
            throw exception(exceptionCode, e, args);
        }
    }

    /**
     * Create the exception object
     * @param exceptionCode The exception to throw
     * @param source The source exception
     * @param args The parameters for the exception message
     * @return The created exception object
     */
    EXCEPTION_TYPE exception(final ExceptionCode exceptionCode, final Throwable source, final Object... args);
}

package fr.phenix246.sherlock.error.wrap;

import fr.phenix246.sherlock.error.exception.ExceptionCode;

import java.util.function.Supplier;

/**
 * Interface defining the different types of exception wrapping
 * @param <EXCEPTION_TYPE> The type of exception to throw.
 */
public interface ExceptionWrapper<EXCEPTION_TYPE extends Throwable> {

    /**
     * Call the function and wrap any exception with the provided one
     * @param action The function to execute
     * @param exceptionCode The exception to throw
     * @param args The parameters for the exception message
     * @return The result of the function
     * @param <T> The result type
     * @throws EXCEPTION_TYPE if another exception is thrown
     */
    <T> T call(Supplier<T> action, final ExceptionCode exceptionCode, final Object... args) throws EXCEPTION_TYPE;

    /**
     * Call the function and wrap any exception with the provided one
     * @param action The function to execute
     * @param exceptionCode The exception to throw
     * @param args The parameters for the exception message
     * @throws EXCEPTION_TYPE if another exception is thrown
     */
    void call(Runnable action, final ExceptionCode exceptionCode, final Object... args) throws EXCEPTION_TYPE;
}

package fr.phenix246.sherlock.error;

import fr.phenix246.sherlock.error.exception.FunctionalException;
import fr.phenix246.sherlock.error.exception.TechnicalException;
import fr.phenix246.sherlock.error.exception.UncheckedException;
import fr.phenix246.sherlock.error.validation.DefaultAssert;

public interface Assertion {

    DefaultAssert<UncheckedException> UNCHECKED = UncheckedException::new;
    DefaultAssert<FunctionalException> FUNCTIONAL = FunctionalException::new;
    DefaultAssert<TechnicalException> TECHNICAL = TechnicalException::new;

}

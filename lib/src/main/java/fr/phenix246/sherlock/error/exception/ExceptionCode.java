package fr.phenix246.sherlock.error.exception;

public interface ExceptionCode {

    int code();

    String message();
}

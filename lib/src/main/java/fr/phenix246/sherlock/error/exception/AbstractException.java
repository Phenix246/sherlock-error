package fr.phenix246.sherlock.error.exception;

public abstract class AbstractException extends Exception {

    private final int errorCode;

    public AbstractException(ExceptionCode code) {
        super(code.message());
        this.errorCode = code.code();
    }

    public AbstractException(final ExceptionCode code, final Throwable origin) {
        super(code.message(), origin);
        errorCode = code.code();
    }

    public AbstractException(final ExceptionCode code, final Object... args) {
        super(String.format(code.message(), args));
        errorCode = code.code();
    }

    public AbstractException(final ExceptionCode code, final Throwable origin, final Object... args) {
        super(String.format(code.message(), args), origin);
        errorCode = code.code();
    }

    public int getErrorCode() {
        return errorCode;
    }
}

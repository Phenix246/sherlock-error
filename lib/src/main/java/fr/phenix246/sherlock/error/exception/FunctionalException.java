package fr.phenix246.sherlock.error.exception;

public class FunctionalException extends AbstractException {

    public FunctionalException(final ExceptionCode code) {
        super(code);
    }

    public FunctionalException(final ExceptionCode code, final Throwable origin) {
        super(code, origin);
    }

    public FunctionalException(final ExceptionCode code, final Object... args) {
        super(code, args);
    }

    public FunctionalException(final ExceptionCode code, final Throwable origin, final Object... args) {
        super(code, origin, args);
    }
}

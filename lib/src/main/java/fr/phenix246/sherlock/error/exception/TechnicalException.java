package fr.phenix246.sherlock.error.exception;

import fr.phenix246.sherlock.error.exception.AbstractException;
import fr.phenix246.sherlock.error.exception.ExceptionCode;

public class TechnicalException extends AbstractException {

    public TechnicalException(final ExceptionCode code) {
        super(code);
    }

    public TechnicalException(final ExceptionCode code, final Throwable origin) {
        super(code, origin);
    }

    public TechnicalException(final ExceptionCode code, final Object... args) {
        super(code, args);
    }

    public TechnicalException(final ExceptionCode code, final Throwable origin, final Object... args) {
        super(code, origin, args);
    }
}

package fr.phenix246.sherlock.error.exception;

public class UncheckedException extends AbstractRuntimeException {

    public UncheckedException(final ExceptionCode code) {
        super(code);
    }

    public UncheckedException(final ExceptionCode code, final Throwable origin) {
        super(code, origin);
    }

    public UncheckedException(final ExceptionCode code, final Object... args) {
        super(code, args);
    }

    public UncheckedException(final ExceptionCode code, final Throwable origin, final Object... args) {
        super(code, origin, args);
    }
}

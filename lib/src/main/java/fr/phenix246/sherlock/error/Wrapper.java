package fr.phenix246.sherlock.error;

import fr.phenix246.sherlock.error.exception.FunctionalException;
import fr.phenix246.sherlock.error.exception.TechnicalException;
import fr.phenix246.sherlock.error.exception.UncheckedException;
import fr.phenix246.sherlock.error.wrap.DefaultExceptionWrapper;

public interface Wrapper {

    DefaultExceptionWrapper<UncheckedException> UNCHECKED = UncheckedException::new;
    DefaultExceptionWrapper<FunctionalException> FUNCTIONAL = FunctionalException::new;
    DefaultExceptionWrapper<TechnicalException> TECHNICAL = TechnicalException::new;
}
